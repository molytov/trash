# Molytov/Trash
Just some miscellaneous trash that doesn't need its own repo.

### Extract-Email
A shellscript that gets email addresses from given files/directory of files.
Usage:
`$ extract-email <dir or file> <another dir or file>`

Inspired from https://github.com/HaveIBeenPwned/EmailAddressExtractor

Hopefully will add handling for errors or storage space issues, support for PDFs, office suite files, archives etc, maybe also having a stats output of some kind (ex. how many unique addresses, most common domains).

### archive-org-email
Archive.org publishes the uploader's registered email address in an upload's _meta.xml file.

### zlibr
Z-Library has an API to fetch working mirrors.
I guess it's not a terrible idea, but if you can make a successful request to a mirror's API, you might as well just use that very mirror to find your downloads.
